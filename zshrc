# zsh functions to grab the current ruby version info from RVM.
# Useful mainly in prompts (see http://skitch.com/oshuma/nni3k/zsh-prompt-git-clean).

# ~/.zshrc
# RPS1=$'Ruby v$(ruby_prompt_version)'

# Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh
export NODE_PATH=/usr/local/lib/node

source $HOME/.aliases
source $HOME/.profile
source $HOME/.ohmygems

bindkey ";5D" backward-word
bindkey ";5C" forward-wordbindkey

# Set to the name theme to load.
# Look in ~/.oh-my-zsh/themes/
# export ZSH_THEME="robbyrussell"
# export ZSH_THEME="Soliah"
# export ZSH_THEME="afowler"
# export ZSH_THEME="arrow" # Simple and nice but the colors are a little light for a light color scheme
# export ZSH_THEME="aussiegeek"
# export ZSH_THEME="awesomepanda"
# export ZSH_THEME="bira" # Not bad for a two liner
# export ZSH_THEME="candy"
# export ZSH_THEME="clean"
# export ZSH_THEME="cloud"
# export ZSH_THEME="cypher"
# export ZSH_THEME="dallas"
# export ZSH_THEME="darkblood"
# export ZSH_THEME="daveverwer" # Not bad for light color scheme
# export ZSH_THEME="dieter"
# export ZSH_THEME="dst"
# export ZSH_THEME="dstufft"
# export ZSH_THEME="duellj"
# export ZSH_THEME="eastwood" # CURRENT_DEFAULT
# export ZSH_THEME="lodestone"
# export ZSH_THEME="edvardm"
# export ZSH_THEME="evan"
# export ZSH_THEME="example"
# export ZSH_THEME="fishy"
# export ZSH_THEME="flazz"
# export ZSH_THEME="fletcherm"
# export ZSH_THEME="funky"
# export ZSH_THEME="gallifrey" # Not bad for light color scheme
# export ZSH_THEME="garyblessington"
# export ZSH_THEME="gentoo"
# export ZSH_THEME="geoffgarside"
# export ZSH_THEME="gozilla"
# export ZSH_THEME="imajes"
# export ZSH_THEME="jbergantine"
# export ZSH_THEME="jnrowe" # Interesting but poor color choices
# export ZSH_THEME="josh" # Contender for light color scheme (two liner)
# export ZSH_THEME="jreese"
# export ZSH_THEME="kardan"
# export ZSH_THEME="kennethreitz"
# export ZSH_THEME="lambda"
# export ZSH_THEME="linuxonly"
# export ZSH_THEME="lukerandall"
# export ZSH_THEME="macovsky"
# export ZSH_THEME="macovsky-ruby"
# export ZSH_THEME="maran"
# export ZSH_THEME="mgutz"
# export ZSH_THEME="mikeh"
# export ZSH_THEME="mrtazz"
# export ZSH_THEME="lodestone"
# export ZSH_THEME="nanotech" # not bad
# export ZSH_THEME="nicoulaj" # Really simple and clean. I like this one.
# export ZSH_THEME="philips"
# export ZSH_THEME="pmcgee"
# export ZSH_THEME="rgm"
# export ZSH_THEME="risto"
# export ZSH_THEME="rixius"
# export ZSH_THEME="robbyrussell"
# export ZSH_THEME="skaro"
# export ZSH_THEME="sorin"
# export ZSH_THEME="sporty_256"
# export ZSH_THEME="tardm"
export ZSH_THEME="mira"
# 
# Set to this to use case-sensitive completion
# export CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# export DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# export DISABLE_LS_COLORS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=( fasd vi-mode autojump git heroku osx brew bundler cap github rbenv rake rails rails3 gem github ftl zsh-syntax-highlighting)
# vi-mode)

source $ZSH/oh-my-zsh.sh

# Un set options
unsetopt correct_all
unsetopt autocd

# alias heroku='nocorrect heroku'
# alias rvm='nocorrect rvm'
# alias gem='nocorrect gem'

#Customize to your needs...
#rt PATH=~/bin:/usr/local/bin:/usr/local/sbin:/usr/local/mysql/bin:$PATH:/sw/bin
#export PATH=$PATH:$EC2_HOME/bin
## MacPorts Installer addition on 2009-09-01_at_06:32:26: adding an appropriate PATH variable for use with MacPorts.
#export PATH=/opt/local/bin:/opt/local/sbin:$PATH
export PATH=~/.cabal/bin:~/bin:/usr/local/git/bin:/usr/local/bin:/usr/local/sbin/:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin:~/.rbenv/bin
# source $HOME/.rvm/scripts/rvm > /dev/null 2>&1
# rvm use 1.9.2> /dev/null 2>&1

############################################################
# Mekentosj ec2
# ######################################################## #
export EC2_PRIVATE_KEY=/Users/matt/.ec2/pk-OVFHOWK3LVRQLVAPDCKSTBLUAM4ZTWXY.pem
export EC2_CERT=/Users/matt/.ec2/cert-OVFHOWK3LVRQLVAPDCKSTBLUAM4ZTWXY.pem
export AWS_CREDENTIAL_FILE=/Users/matt/.ec2/credential_file
export JAVA_HOME=/Library/Java/Home
export EC2_HOME="/usr/local/Cellar/ec2-api-tools/1.5.2.3/jars"
export EC2_AMITOOL_HOME="/usr/local/Cellar/ec2-ami-tools/1.3-45758/jars"
export AWS_RDS_HOME="/usr/local/Cellar/rds-command-line-tools/1.3.003/jars"

# Init rbenv
# Force SHELL variable. For some reason $SHELL is bash even though we are in zsh
export SHELL=/bin/zsh
eval "$(rbenv init -)"

# if [ -f `brew --prefix`/etc/autojump ]; then
#   . `brew --prefix`/etc/autojump
# fi

export EDITOR=vim
export HISTSIZE=100000 
export SAVEHIST=100000 
export HISTFILE=~/.zhistory


# source ~/.oh-my-zsh/custom/plugins/auto-fu/auto-fu.zsh
# zle-line-init () {auto-fu-init;}; zle -N zle-line-init
# zstyle ':completion:*' completer _oldlist _complete
# zle -N zle-keymap-select auto-fu-zle-keymap-select

# Initialize Node Version Manager
source ~/.nvm/nvm.sh
# source /usr/local/etc/profile.d/z.sh


function precmd() {
    if [ "$(id -u)" -ne 0 ]; then
        FULL_CMD_LOG="$HOME/.logs/zsh-history-$(date -u "+%Y-%m-%d").log"
        echo "$USER@`hostname`:`pwd` [$(date -u)] `\history -1`" >> ${FULL_CMD_LOG}
    fi
}
export PATH="$HOME/.deliver/bin:$PATH"


# VI MODE KEYBINDINGS (ins mode)
bindkey -M viins '^a'    beginning-of-line
bindkey -M viins '^e'    end-of-line
bindkey -M viins '^k'    kill-line
bindkey -M viins '^r'    history-incremental-pattern-search-backward
bindkey -M viins '^s'    history-incremental-pattern-search-forward
bindkey -M viins '^p'    up-line-or-history
bindkey -M viins '^n'    down-line-or-history
bindkey -M viins '^y'    yank
bindkey -M viins '^w'    backward-kill-word
bindkey -M viins '^u'    backward-kill-line
bindkey -M viins '^h'    backward-delete-char
bindkey -M viins '^?'    backward-delete-char
bindkey -M viins '^_'    undo
bindkey -M viins '^x^r'  redisplay
bindkey -M viins '\eOH'  beginning-of-line # Home
bindkey -M viins '\eOF'  end-of-line       # End
bindkey -M viins '\e[2~' overwrite-mode    # Insert
bindkey -M viins '\ef'   forward-word      # Alt-f
bindkey -M viins '\eb'   backward-word     # Alt-b
bindkey -M viins '\ed'   kill-word         # Alt-d                    
bindkey -M viins '^d'    delete-char
bindkey -M viins '^[t'   transpose-words   # Alt+t

# VI MODE KEYBINDINGS (cmd mode)
bindkey -M vicmd '^a'    beginning-of-line
bindkey -M vicmd '^e'    end-of-line
bindkey -M vicmd '^k'    kill-line
bindkey -M vicmd '^r'    history-incremental-pattern-search-backward
bindkey -M vicmd '^s'    history-incremental-pattern-search-forward
bindkey -M vicmd '^p'    up-line-or-history
bindkey -M vicmd '^n'    down-line-or-history
bindkey -M vicmd '^y'    yank
bindkey -M vicmd '^w'    backward-kill-word
bindkey -M vicmd '^u'    backward-kill-line
bindkey -M vicmd '/'     vi-history-search-forward
bindkey -M vicmd '?'     vi-history-search-backward
bindkey -M vicmd '^_'    undo
bindkey -M vicmd '\ef'   forward-word                      # Alt-f
bindkey -M vicmd '\eb'   backward-word                     # Alt-b
bindkey -M vicmd '\ed'   kill-word                         # Alt-d
bindkey -M vicmd '\e[5~' history-beginning-search-backward # PageUp
bindkey -M vicmd '\e[6~' history-beginning-search-forward  # PageDown

# More emac-sy goodness
bindkey '^[^[[D'    emacs-backward-word
bindkey '^[^[[C'    emacs-forward-word
bindkey '^[[1;5D'    beginning-of-line
bindkey '^[[1;5C'    end-of-line

# Open command line in $EDITOR (vim)
bindkey -M vicmd '^x^e' edit-command-line
bindkey -M vicmd v edit-command-line

