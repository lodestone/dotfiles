# Mira ZSH Theme 

# Customizable parameters.
PROMPT_PATH_MAX_LENGTH=30
PROMPT_DEFAULT_END=•
PROMPT_ROOT_END=❯❯❯
PROMPT_SUCCESS_COLOR=$FG[071]
PROMPT_FAILURE_COLOR=$FG[124]
PROMPT_VCS_INFO_COLOR=$FG[242]

# Set required options.
setopt promptsubst

# Load required modules.
autoload -U add-zsh-hook
autoload -Uz vcs_info

# Node info function
function node_info {
  local node_version=$(node --version)
  # local node_version=$(node -e "var execSync = require('execSync'); var v = execSync.stdout('node --version').toString().trim(); console.log(v);")
  if which node &> /dev/null; then
    NVM_NODE=%{$fg[blue]%}‹$node_version›%{$reset_color%}
  fi
}

# Add hook for calling vcs_info before each command.
add-zsh-hook precmd vcs_info
# add-zsh-hook precmd node_info

# Set vcs_info parameters.
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*:*' check-for-changes true # Can be slow on big repos.
zstyle ':vcs_info:*:*' unstagedstr '!'
zstyle ':vcs_info:*:*' stagedstr '+'
zstyle ':vcs_info:*:*' actionformats "%S" "%r[%b] %u%c (%a)"
zstyle ':vcs_info:*:*' formats "%S" "%r[%b] %u%c"
zstyle ':vcs_info:*:*' nvcsformats "%~" ""

local return_code="%(?..%{$fg[red]%}%? ↵%{$reset_color%})"

local user_host='%{$terminfo[bold]$fg[green]%}%n@%m%{$reset_color%}'
# local current_dir='%{$terminfo[bold]$fg[magenta]%} %~%{$reset_color%}'
local current_dir='%{$fg[magenta]%} %~%{$reset_color%}'
local rvm_ruby=''
if which rvm-prompt &> /dev/null; then
  rvm_ruby='%{$fg[red]%}‹$(rvm-prompt i v g)›%{$reset_color%}'
else
  if which rbenv &> /dev/null; then
    rvm_ruby='%{$fg[red]%}‹$(rbenv version | sed -e "s/ (set.*$//")›%{$reset_color%}'
  fi
fi
local git_branch='$(git_prompt_info)%{$reset_color%}'

local nvm_node='$NVM_NODE'

# NOT WORKING: Doesn't update when I cd
# function prompt_char {
#     git branch >/dev/null 2>/dev/null && echo '±' && return
#     hg root >/dev/null 2>/dev/null && echo '☿' && return
#     echo '○'
# }

#################### PROMPT #############################################
# PROMPT="${user_host} ${current_dir} ${rvm_ruby} ${nvm_node} %{$PROMPT_VCS_INFO_COLOR%}${git_branch}%{$reset_color%}
PROMPT="${user_host} ${current_dir} ${rvm_ruby} %{$PROMPT_VCS_INFO_COLOR%}${git_branch}%{$reset_color%}
%{$PROMPT_DEFAULT_END%} "
# $(prompt_char) "
# PROMPT="%(0?.%{$PROMPT_SUCCESS_COLOR%}.%{$PROMPT_FAILURE_COLOR%})${SSH_TTY:+[%n@%m]}%{$FX[bold]%}%$PROMPT_PATH_MAX_LENGTH<..<"'${vcs_info_msg_0_%%.}'"%<<%(!.$PROMPT_ROOT_END.$PROMPT_DEFAULT_END)%{$FX[no-bold]%}%{$FX[reset]%} "
# RPROMPT="%{$PROMPT_VCS_INFO_COLOR%}"'$vcs_info_msg_1_'"%{$FX[reset]%}"
# PROMPT="${user_host} $PROMPT_ROOT_END.$PROMPT_DEFAULT_END"
# RPROMPT="%{$PROMPT_VCS_INFO_COLOR%}"'$vcs_info_msg_1_'"%{$FX[reset]%}"
# RPROMPT="rprompt"

RPS1="${return_code}"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[yellow]%}‹"
ZSH_THEME_GIT_PROMPT_SUFFIX="› %{$reset_color%}"
