# require "awesome_print"

require "irb/ext/save-history"
require 'irb/completion'

begin
  require 'active_record'
  module ActiveRecord
    class Base
      def self.[](*args)
        if args.first.is_a?(Regexp)
          puts "Regex"
          column_names.each {|c| p c.class }      
        else
          find(args)
        end
      end
    end
  end
rescue LoadError
end

IRB.conf[:SAVE_HISTORY] = 20000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-history"

if defined? Rails
  if Rails.env
    # Set up the prompt to be slightly more informative
    rails_env = Rails.env.test? ? 'test' : Rails.env.downcase[0,3]
    current_app = Dir.pwd.split('/').last
    IRB.conf[:PROMPT].reverse_merge!(:RAILS_ENV => {:PROMPT_I=>"#{current_app} #{rails_env} >> ", :PROMPT_N=>"#{current_app} #{rails_env} >> ", :PROMPT_S=>nil, :PROMPT_C=>"?> ", :RETURN=>"=> %s\n"})
    IRB.conf[:PROMPT_MODE] = :RAILS_ENV
  end
end

puts "__--~~ loaded ~/.irbrc ~~--__" 
